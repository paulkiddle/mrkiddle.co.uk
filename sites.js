const domains = {
	blog: 'blog.mrkiddle.co.uk',
	recipebook: 'recipes.kith.kitchen',
	shack: 'shack.mrkiddle.co.uk',
	links: 'links.mrkiddle.co.uk',
	gorse: 'gorse.hades.town',
};
export default domains;
