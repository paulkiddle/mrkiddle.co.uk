import argon2 from 'argon2';
import getDatabase from './src/data/database/create.js';

const pwd = process.argv[2];
const db = await getDatabase('./db.sqlite');
const hash = await argon2.hash(pwd);

await db.run`INSERT INTO credentials (username, password, profile_id) VALUES
('paul', ${hash}, 1);`;
