import http from 'node:http';
import path from 'node:path';

const database = process.argv[2];
const staticDir = process.argv[3] ?? '/var/www';
const nodeDir = process.argv[4] ?? path.resolve(staticDir, 'node');
const hosts = {
	blog: 'blog.mrkiddle.co.uk',
	root: 'mrkiddle.co.uk',
};

const { default: Server } = await import(path.resolve(nodeDir, 'main.js'));
const partyFile = path.join(staticDir, 'a-party.xyz/index.html');

const handler = await Server.create(partyFile, database, hosts);

http.createServer(handler).listen(process.env.PORT);
