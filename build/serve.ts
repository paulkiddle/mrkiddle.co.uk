import http, { type IncomingMessage, type ServerResponse } from 'node:http';
import { type AddressInfo } from 'node:net';
import serve from 'serve-handler';
import Server from '../src/main.js';

const dir = new URL('../static', import.meta.url).pathname;

const domains = [
	'a-party.xyz',
	'mrkiddle.co.uk',
	'blog.mrkiddle.co.uk',
];

const localDomains = domains.map(d => d + '.localhost');

const port = 3030;
const hosts = {
	root: localDomains[1],
	blog: localDomains[2],
	http: true,
	port: `:${port}`,
} as const;
const app = await Server.create(dir + '/a-party.xyz/index.html', 'db.sqlite', hosts);

async function serveStatic(request: IncomingMessage, res: ServerResponse, pub: string) {
	try {
		await serve(request, res, { public: pub, directoryListing: false }, {
			async sendError(...args) {
				const spec = args.at(-1);
				throw spec;
			},
		});
		return true;
	} catch (error) {
		if ((error as { code: string }).code === 'not_found') {
			return false;
		}

		throw error;
	}
}

async function serveStaticDomain(request: IncomingMessage, res: ServerResponse) {
	const d = localDomains.indexOf((request.headers.host ?? '').split(':')[0]);
	if (d > -1) {
		return serveStatic(request, res, dir + '/' + domains[d]);
	}

	res.end(localDomains.map(d => 'http://' + d).join('\n'));
	return true;
}

const server = http.createServer(async (request: IncomingMessage, res: ServerResponse) => {
	try {
		if (await serveStaticDomain(request, res)) {
			return;
		}

		if (await app(request, res)) {
			return;
		}
	} catch (error) {
		console.log(error);
		res.statusCode = 500;
		res.end((error as Error).message);
	}
});

await new Promise(res => server.listen(port, () => {
	res(null);
}));

for (const d of localDomains) {
	console.log(`http://${d}:` + (server.address() as AddressInfo).port);
}
