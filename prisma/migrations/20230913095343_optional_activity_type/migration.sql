-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_InboxActivity" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "ownerId" INTEGER NOT NULL,
    "activityId" INTEGER NOT NULL,
    CONSTRAINT "InboxActivity_ownerId_fkey" FOREIGN KEY ("ownerId") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "InboxActivity_activityId_fkey" FOREIGN KEY ("activityId") REFERENCES "Activity" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_InboxActivity" ("activityId", "id", "ownerId") SELECT "activityId", "id", "ownerId" FROM "InboxActivity";
DROP TABLE "InboxActivity";
ALTER TABLE "new_InboxActivity" RENAME TO "InboxActivity";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
