/*
  Warnings:

  - Added the required column `uri` to the `Activity` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Activity" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "uri" TEXT NOT NULL,
    "body" TEXT NOT NULL,
    "actorId" INTEGER NOT NULL,
    "type" TEXT,
    CONSTRAINT "Activity_actorId_fkey" FOREIGN KEY ("actorId") REFERENCES "Actor" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Activity" ("actorId", "body", "id", "type") SELECT "actorId", "body", "id", "type" FROM "Activity";
DROP TABLE "Activity";
ALTER TABLE "new_Activity" RENAME TO "Activity";
CREATE UNIQUE INDEX "Activity_uri_key" ON "Activity"("uri");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
