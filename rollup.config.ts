// rollup.config.js
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import nodeExternals from 'rollup-plugin-node-externals'
import { importMetaAssets } from '@web/rollup-plugin-import-meta-assets';
import json from '@rollup/plugin-json';
import { getAssetPathFromObject } from './src/config-helpers.js';


export default {
  input: 'src/main.ts',
  output: {
    dir: 'dist',
    assetFileNames: getAssetPathFromObject
  },
  plugins: [
    typescript(),
    json(),
    nodeExternals(),
    nodeResolve({
      exportConditions: ['node']
    }),
    commonjs(),
    importMetaAssets()
  ]
};