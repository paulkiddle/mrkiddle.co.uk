/// <reference path="../src/activitypub-http-signatures.d.ts"/>
import http from 'http';
import { Sha256Signer} from 'activitypub-http-signatures';

const port = 3003;
const origin = `http://test.localhost:${port}`;
const target = 'http://mrkiddle.co.uk.localhost:3030/@a';

type Opts = {
    method?: string,
    headers?: Record<string, string>
};

async function request(url: string, opts: Opts = {}, body?: string){
    const headers = opts.headers || {};
    const req = {
        ...opts,
        headers: {
            ...headers,
            accept: 'application/activity+json',
            'content-type':'application/activity+json'
        }
    }
    return new Promise<http.IncomingMessage>((resolve, reject) => {
        const r = http.request(url, req).on('response', r=> {
            resolve(r);
        }).on('error', reject);
        r.end(body);
    });
}

async function sendTestActivity(port: number, origin: string, targetActor :string) {
    const keys = {
    private: `-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBAKC9U+CuSjPm/nX9XZ+9S1JDHHCYJWpks9qJ//GLdtrmOWlhdc3C
ED7/97c12Fa6ZTnii/nLUzmz5Z5kMQ5CLJUCAwEAAQJBAJG3kRaaakJrIjusmPd7
D5FfraSVCTZOXI29lP1QRUtjAtJaPDD058HOSJM49xmbGDPaTUlu01B1nA8rcByV
3fECIQDMhNCDrsV6g0Utd9gPSaa4lHKShgRt0mO8NOHDhlvpswIhAMkzYULfiawX
6fiEwAkNjMxY1OsUNIdEFGL1WAgMNVyXAiBfa6QhnDxU4cQ3549t6o4X1mLyTAbq
+ltAJ2giIqDlkwIgMYXPA8nHti8wrLXoGpJWPJoE1lPj1gOAzRa8c0al/8MCIQCh
WS/vhJsQmetk9fTzNeT6I+LIwpkxkerpXoyebUNMdg==
-----END RSA PRIVATE KEY-----`,
    public: `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKC9U+CuSjPm/nX9XZ+9S1JDHHCYJWpk
s9qJ//GLdtrmOWlhdc3CED7/97c12Fa6ZTnii/nLUzmz5Z5kMQ5CLJUCAwEAAQ==
-----END PUBLIC KEY-----`
    };

    const user = 'test';
    const testUrl = new URL(`/@${user}`, origin);
    const keyId = testUrl.href+"#main-key";

    const testProfile = {
        "@context":["https://www.w3.org/ns/activitystreams","https://w3id.org/security/v1"],
        "id":testUrl.href,"type":"Person","http://joinmastodon.org/ns#discoverable":true,
        "inbox": testUrl.href + "/inbox",
        "publicKey":
            {"id":keyId,
            "owner":testUrl.href,
            "publicKeyPem":keys.public},
        "as:manuallyApprovesFollowers":true,
        "name":user,"preferredUsername":user,"url":testUrl.href}

        console.log(JSON.stringify(testProfile));
        
    const s = http.createServer((req, res) => {
        if(req.url === testUrl.pathname) {
            res.end(JSON.stringify(testProfile));
        } else {
            console.log(404, req.url);
            res.statusCode = 404;
            res.end();
        }
    });

    s.listen(port);

    const signer = new Sha256Signer({ publicKeyId: keyId , privateKey: keys.private });

    console.log('req...', targetActor);

    const actorr = await request(targetActor);
    let body = '';
    for await(const chunk of actorr) {
        body += chunk;
    }
    console.log(body);
    const actor = JSON.parse(body);

    const targetInbox = actor.inbox;

    const req = {
        url: targetInbox,
        method: 'POST',
        headers: {
            accept: 'application/activity+json',
            'content-type':'application/activity+json'
        } as Record<string, string>
    };
    req.headers.signature = signer.sign(req);

    const r = await request(req.url, req, JSON.stringify({
        "@context":["https://www.w3.org/ns/activitystreams","https://w3id.org/security/v1"],
        id: 'ex:activity',
        type: 'Follow',
        "actor": {
            id: testUrl.href
        },
        published: '2023-08-18T08:02:53Z',
        to: "https://www.w3.org/ns/activitystreams#Public",
        cc: "https://www.w3.org/ns/activitystreams#Public",
        object: {
            id: actor.id
        }
    }));
    
    console.log(r.statusCode);
    r.pipe(process.stdout);

   // s.close();
}

console.log(1)
await sendTestActivity(port, origin, target);
console.log(2)
