import { default as sqlite3, type Statement } from 'sqlite3';
import sql, { type Sql } from 'sql-template-tag';
import { type Id, type ProfileRecord, type UserRecord } from './migrate.js';

type QueryArgs = [Sql] | [TemplateStringsArray, ...(string|number|undefined|boolean|null|Buffer|Id)[]];

const querify = (...[a0, ...args]: QueryArgs) => 'sql' in a0 ? a0 : sql(a0, ...args);

type ChangeResult = {
	lastID: Id;
	changes: number;
};

export default class Database {
	#db: sqlite3.Database;

	constructor(path: string) {
		this.#db = new sqlite3.Database(path);
	}

	async run(...args: QueryArgs) {
		const query = querify(...args);

		return await new Promise<ChangeResult>((resolve, reject) => this.#db.run(query.sql, query.values, function (e) {
			if (e) {
				reject(e);
			} else {
				resolve({ lastID: this.lastID as Id, changes: this.changes });
			}
		}));
	}

	async insert(...args: QueryArgs) {
		const { lastID } = await this.run(...args);
		return lastID;
	}

	async get<T>(...args: QueryArgs) {
		const fn = this.get;
		const query = querify(...args);
		return new Promise<T | null>((resolve, reject) => this.#db.get<T>(query.sql, query.values, (e, result) => {
			if (e) {
				Error.captureStackTrace(e, fn);
				reject(e);
			} else {
				resolve(result);
			}
		}));
	}

	async * each<T>(...args: QueryArgs) {
		const fn = this.each;
		const query = querify(...args);
		const statement = await new Promise<Statement>((resolve, reject) => {
			const statement = this.#db.prepare(query.sql, query.values, e => {
				if (e) {
					Error.captureStackTrace(e, fn);
					reject(e);
				} else {
					resolve(statement);
				}
			});
		});

		try {
			let next: T | null;

			do {
				next = await new Promise<T | null>((resolve, reject) => {
					statement.get<T>((err, row) => {
						if (err) {
							reject(err);
						} else {
							resolve(row ?? null);
						}
					});
				});
				if (next) {
					yield next;
				}
			} while (next);
		} finally {
			await new Promise((resolve, reject) => statement.finalize(e => {
				if (e) {
					reject(e);
				} else {
					resolve(null);
				}
			}));
		}
	}

	async transaction(fn: () => unknown, pragmaOff = false) {
		if (pragmaOff) {
			await this.run`PRAGMA foreign_keys=OFF`;
		}

		await this.run`BEGIN`;

		try {
			await fn();
			await this.run`COMMIT`;
		} catch (error) {
			await this.run`ROLLBACK`;

			Error.captureStackTrace(error as object, fn);
			throw error;
		} finally {
			if (pragmaOff) {
				await this.run`PRAGMA foreign_keys=ON`;
			}
		}
	}
}

export * from './migrate.js';
