import { type Buffer } from 'node:buffer';
import keypair from 'keypair';
import type Database from './database.js';

/* eslint-disable no-fallthrough */
export default async function migrate(db: Database, version: number) {
	switch (version) {
		case 0: {
			await db.run`CREATE TABLE profiles (
				id	INTEGER	NOT NULL	PRIMARY KEY,
				name	STRING	NOT NULL	UNIQUE,
				local	INTEGER	NOT NULL	DEFAULT 1,
				public_key	BLOB
			);`;
			await db.run`CREATE TABLE users (
				id	INTEGER	NOT NULL	PRIMARY Key,
				password	STRING 	NOT NULL,
				admin	INTEGER	NOT NULL	DEFAULT 0,
				profile_id	INTEGER	NOT NULL	UNIQUE,
			
				FOREIGN KEY(profile_id) REFERENCES profiles(id)
			);`;

			await db.run`CREATE TABLE sessions (
				id	INTEGER NOT NULL	PRIMARY Key,
				uuid	STRING  NOT NULL	,
				user_id	INTEGER NOT NULL	,
			
				FOREIGN KEY(user_id) REFERENCES users(id)
			);`;

			await db.run`CREATE TABLE invites (
				id	INTEGER	NOT NULL	PRIMARY KEY,
				uuid	STRING	NOT NULL,
				admin	INTEGER	NOT NULL,
				quantity	INTEGER,
				used	INTEGER	NOT NULL	DEFAULT 0,
				creator_id	INTEGER,
		
				FOREIGN KEY(creator_id) REFERENCES users(id)
			);`;
		}

		case 1:
		case 2: {
			await db.transaction(async () => {
				await db.run`CREATE TABLE x_profiles (
					id	INTEGER	NOT NULL	PRIMARY KEY,
					name	STRING	NOT NULL	UNIQUE,
					local	INTEGER	NOT NULL	DEFAULT 1,
					public_key	BLOB	NOT NULL
				);`;

				await db.run`CREATE TABLE x_users (
					id	INTEGER	NOT NULL	PRIMARY KEY,
					password	STRING 	NOT NULL,
					admin	INTEGER	NOT NULL	DEFAULT 0,
					profile_id	INTEGER	NOT NULL	UNIQUE,
					private_key	BLOB	NOT NULL,
				
					FOREIGN KEY(profile_id) REFERENCES profiles(id)
				);`;

				type Row = Record<'profile_id' | 'name' | 'user_id' | 'password', string | number>;
				for await (const row of db.each<Row>`SELECT u.id as user_id, u.*, p.* FROM users AS u LEFT JOIN profiles AS p on u.profile_id=p.id`) {
					const key = keypair();
					await db.run`INSERT INTO x_profiles (id, name, local, public_key) VALUES(${row.profile_id}, ${row.name}, 1, ${key.public})`;
					await db.run`INSERT INTO x_users (id, password, admin, profile_id, private_key) VALUES(${row.user_id}, ${row.password}, 1, ${row.profile_id}, ${key.private})`;
				}

				await db.run`DROP TABLE profiles`;
				await db.run`DROP TABLE users`;

				await db.run`ALTER TABLE x_profiles RENAME TO profiles`;
				await db.run`ALTER TABLE x_users RENAME TO users`;

				await db.run`PRAGMA foreign_key_check`;
			}, true);
		}

		case 3: {
			await db.run`CREATE TABLE domains (
				id	INTEGER	NOT NULL	PRIMARY KEY,
				hostname	STRING	NOT NULL	UNIQUE,
				origin	STRING	NOT NULL	UNIQUE,
				local	INTEGER	NOT NULL	DEFAULT 0,
				banned	INTEGER	NOT NULL	DEFAULT 0
			)`;
			const { lastID } = await db.run`INSERT INTO domains (hostname, origin, local) VALUES ('', '', 1)`;

			await db.transaction(async () => {
				await db.run`CREATE TABLE x_profiles (
					id	INTEGER	NOT NULL	PRIMARY KEY,
					name	STRING	NOT NULL	UNIQUE,
					public_key	BLOB	NOT NULL,
					domain_id	INTEGER	NOT NULL,
				
					FOREIGN KEY(domain_id)	REFERENCES domains(id)
				);`;

				await db.run`INSERT INTO x_profiles SELECT id,name,public_key, ${lastID} AS domain_id FROM profiles WHERE true`;

				await db.run`DROP TABLE profiles`;

				await db.run`ALTER TABLE x_profiles RENAME TO profiles`;

				await db.run`PRAGMA foreign_key_check`;
			}, true);

			await db.run`CREATE TABLE activities (
				id	INTEGER 	NOT NULL	PRIMARY KEY,
				body	STRING	NOT NULL,
				actor_id	INTEGER	NOT NULL,

				FOREIGN KEY(actor_id) REFERENCES actors(id)
			)`;

			await db.run`CREATE TABLE inbox_activities (
				id	INTEGER	NOT NULL	PRIMARY KEY,
				owner_id	INTEGER	NOT NULL,
				activity_id	INTEGER NOT NULL,

				FOREIGN KEY(owner_id)	REFERENCES profiles(id),
				FOREIGN KEY(activity_id)	REFERENCES activities(id)
			)`;
		}

		case 4: {
			await db.run`
				ALTER TABLE activities ADD COLUMN type TEXT;
			`
		}
	}

	return 5;
}

/* eslint-enable no-fallthrough */

export type Id<T=''> = number// & { __table: T };

type Prefix<Record, P extends string =''> = {
	[K in keyof Record as K extends string ? `${P}${K}` : never]: Record[K]
};

export type Join<Root, Joinee, P extends string=''> = Root & Prefix<Joinee, P>;

export type SessionRecord = {
	uuid: string;
	user_id: Id;
};

export type InviteRecord = {
	id: Id;
	uuid: string;
	admin: boolean;
	quantity: number | null;
	used: number;
};

export type ProfileRecord = {
	id: Id
	name: string;
	publicKey: Buffer;
};

export type LocalActorRecord = ProfileRecord & {
	local: true;
	user: {
		id: Id
	}
}

export type RemoteActorRecord = ProfileRecord & {
	local: false;
	remoteActor: {
		uri: string;
	}
}

export type ProfileWithHost = ProfileRecord & Pick<DomainRecord, 'origin' | 'hostname'>;

export type ActorRecord = LocalActorRecord | RemoteActorRecord;

export type DomainRecord = {
	id: Id;
	hostname: string;
	origin: string;
	local: 1 | 0;
	banned: 1 | 0;
};

export type UserRecord<T extends string=''> = Prefix<{
	id: Id;
	password: string;
	privateKey: Buffer;
}, T>;

type ActivityTypes = 'Follow' | null;

export type ActivityRecord<T extends ActivityTypes = ActivityTypes> = {
	id: Id;
	body: string;
	actorId: Id;
	type: T;
	uri: string;
};
