import { ActorRecord, ActivityRecord, LocalActorRecord } from '../database.js';
import Inbox, { InboxInterface, InboxWithUri } from '../../factory/users/inbox/inbox.js';
import RemoteInbox from '../../factory/users/inbox/remote-inbox.js';
import GuestModelFactory, { ModelFactoryArgs, Uri, isFollowActiviy } from './guest-model-factory.js';
import { PrismaClient } from '@prisma/client';
import { Activity } from './model-factory-interface.js';
import Follow from '../../factory/activities/follow.js';
import LocalUser from '../../factory/users/user.js';
import { RemoteUser } from '../../activitypub/factory.js';

export default class UserModelFactory extends GuestModelFactory {
	#data: PrismaClient;
	#uri: Uri
	#actor: LocalUser|RemoteUser;

	constructor(d: ModelFactoryArgs, actor: LocalUser|RemoteUser) {
		super(d)
		this.#actor = actor;
		this.#uri = d.uri;
		this.#data = d.db;
	}

	inbox(record: ActorRecord): InboxInterface {
		if(record.local) {
			return this.localInbox(record);
		} else {
			return new RemoteInbox(this.#actor, new URL(record.remoteActor.uri));
		}
	}

	localInbox(record: LocalActorRecord): InboxWithUri {
		return new Inbox({ db: this.#data, mf: this, actor: this.#actor, uri: this.#uri }, record, this.#uri.for('inbox', { username: record.name }));
	}

	activity(record: ActivityRecord & { actor: ActorRecord }, object: string): Activity {
		if(isFollowActiviy(record) && this.#actor.local) {
			return new Follow({ mf: this }, this.#actor, record, object);
		}

		return super.activity(record, object);
	}
}
