import { type ProfileRecord, type ActivityRecord, type Id, type ProfileWithHost, type InviteRecord, ActorRecord, LocalActorRecord, UserRecord } from '../database.js';
import Party from '../../factory/party.js';
import type Profile from '../../factory/users/profile.js';
import { type Domain } from '../../factory/users/profile.js';
import { InboxInterface, InboxWithUri } from '../../factory/users/inbox/inbox.js';
import Invite from '../../factory/invites/invite.js';
import User from '../../factory/users/user-interface.js';
import LocalUser, { LocalProfile } from '../../factory/users/user.js';

export type ActivityType = null | 'Follow' | 'Accept';

type Extend<A, B> = B extends null ? A : A & B;

type BaseActivity<T extends ActivityType = null, O = null> = Extend<{
	uri: URL;
	actor: Profile;
	body?: string;
	type: T;
}, O extends null ? O : {
	object: O
}>;

export type FollowActivity = BaseActivity<'Follow', { uri: URL }> & { accept: ()=>Promise<unknown> };
export type AcceptActivity = BaseActivity<'Accept', { uri: URL }>;
export type Activity = BaseActivity | FollowActivity | AcceptActivity;

export type Model = Activity | Profile | Domain;

export default interface ModelFactory {
	party(): Party

	inbox(record: ActorRecord) : InboxInterface

	localInbox(record: LocalActorRecord) : InboxWithUri

	actor(record: ActorRecord): Profile 

	localActor(record: LocalActorRecord): LocalProfile

	getId(model: Model): Id|undefined

	activity(record: ActivityRecord & { actor: ActorRecord }, object: string): Activity 

	invite(record: InviteRecord): Invite

	user(record: UserRecord & { actor: ProfileRecord }): LocalUser
}
