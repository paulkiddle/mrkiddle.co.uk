import { type Compile } from '../../../router.js';
import { type ProfileRecord, type ActivityRecord, type Id, type InviteRecord, LocalActorRecord, ActorRecord, UserRecord } from '../database.js';
import Party from '../../factory/party.js';
import type Profile from '../../factory/users/profile.js';
import { InboxBasic, InboxInterface, InboxWithUri } from '../../factory/users/inbox/inbox.js';
import Invite from '../../factory/invites/invite.js';
import ModelFactory, { Model, Activity } from './model-factory-interface.js';
import { PrismaClient } from '@prisma/client';
import Follow from '../../factory/activities/follow.js';
import User from '../../factory/users/user-interface.js';
import LocalUser, { LocalProfile } from '../../factory/users/user.js';


export class Uri {
	#host: string;
	#compile: Compile;

	constructor(host: string, compile: Compile) {
		this.#host = host;
		this.#compile = compile;
	}

	for<T extends Parameters<Compile>>(name: T[0], args: T[1]) {
		return new URL((this.#compile as (...args: any[]) => string)(name, args), this.#host);
	}
}

export type IdMap = WeakMap<Model, Id>;
export type ModelFactoryArgs = {
	db: PrismaClient;
	uri: Uri;
	origin: string;
	idm: IdMap
};

export const isFollowActiviy = <T>(a: ActivityRecord & T): a is ActivityRecord<'Follow'> & T => a.type === 'Follow';

export default class GuestModelFactory implements ModelFactory {
	#data: PrismaClient;
	#uri: Uri;
	#idMap: IdMap;

	constructor({ db, uri, idm }: ModelFactoryArgs) {
		this.#data = db;
		this.#uri = uri;
		this.#idMap = idm
	}

	party(): Party {
		throw new Error('Forbidden')
	}

	inbox(record: ActorRecord): InboxInterface {
		if(record.local) {
			return this.localInbox(record);
		}
		throw new Error('Not Implemented');
	}

	localInbox(record: LocalActorRecord): InboxWithUri {
		return new InboxBasic(this.#uri.for('inbox', { username: record.name }));
	}

	actor(record: ActorRecord): Profile {
		if(record.local) {
			return this.localActor(record);
		}

		const uri = new URL(record.remoteActor.uri);
	
		return {
			uri,
			name: record.name,
			inbox: this.inbox(record),
			key: {
				uri: new URL('#main-key', uri),
				public: record.publicKey,
			},
		};
	}
	
	localActor(record: LocalActorRecord): LocalProfile {
		return new LocalProfile({ mf: this, uri: this.#uri }, record);
	}

	#id<T extends Model>(model: T, id: Id) {
		this.#idMap.set(model, id);
		return model;
	}

	getId(model: Model) {
		return this.#idMap.get(model);
	}

	activity(record: ActivityRecord & { actor: ActorRecord }, object: string): Activity {
		const basicType = {
			body: record.body,
			actor: this.actor(record.actor),
			type: null,
			uri: new URL(record.uri)
		}
	
		const activity = isFollowActiviy(record) ? new Follow({ mf: this }, null, record, object) : basicType;

		return this.#id(activity, record.id);
	}

	invite(record: InviteRecord) {
		return new Invite(this.#data, record);
	}

	user(record: UserRecord & { actor: LocalActorRecord }): LocalUser {
		return this.#id(new LocalUser({
			mf: this,
			uri: this.#uri
		}, record), record.id);
	}
}
