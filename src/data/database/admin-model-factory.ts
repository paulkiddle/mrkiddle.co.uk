import { type Compile } from '../../router.js';
import type Database from '../database/database.js';
import { type ProfileRecord, type ActivityRecord, type Id, type DomainRecord, type ProfileWithHost, type InviteRecord } from '../database/database.js';
import Party from '../factory/party.js';
import Session from '../factory/sessions/session.js';
import type Profile from '../factory/users/profile.js';
import { type Domain } from '../factory/users/profile.js';
import Inbox from '../factory/users/inbox/inbox.js';
import Invite from '../factory/invites/invite.js';

export type Activity = {
	actor: Pick<Profile, 'uri'>;
	body: string;
};

type Model = Activity | Session | Profile | Domain;

const idm = new WeakMap<Model, Id>();

export default class ModelFactory {
	#data: Database;
	#party: string;
	#host: string;
	#compile: Compile;
	#idMap = idm;
	#actor: Profile | null;
	#localDomain: Id;

	constructor({ d, p, c, h, l }: { d: Database; p: string; h: string; c: Compile; l: Id }, actor: Profile | null) {
		this.#data = d;
		this.#party = p;
		this.#host = h;
		this.#compile = c;
		this.#actor = actor;
		this.#localDomain = l;
	}

	#getUri<T extends Parameters<Compile>>(name: T[0], args: T[1]) {
		return new URL((this.#compile as (...args: any[]) => string)(name, args), this.#host);
	}

	withActor(profile: Profile): ModelFactory {
		const o = {
			d: this.#data,
			p: this.#party,
			h: this.#host,
			c: this.#compile,
			l: this.#localDomain,
		};
		return new ModelFactory(o, profile);
	}

	session(record: ProfileWithHost): Session {
		const profile = this.profile(record);
		const mf = this.withActor(profile);
		return new Session(this.#data, mf, record);
	}

	party() {
		return new Party(this.#party);
	}

	inbox(record: ProfileRecord): Inbox {
		return new Inbox({ db: this.#data, mf: this, actor: this.#actor }, record, this.#getUri('inbox', { username: record.name }));
	}

	#userUri(username: string) {
		return this.#getUri('user', { username });
	}

	profile(record: ProfileWithHost): Profile {
		const uri = this.#userUri(record.name);
		return {
			uri,
			name: record.name,
			inbox: this.inbox(record),
			domain: {
				uri: new URL(record.origin),
				hostname: record.hostname,
			},
			key: {
				uri: new URL('#main-key', uri),
				public: record.public_key,
			},
		};
	}

	#id<T extends Model>(model: T, id: Id) {
		this.#idMap.set(model, id);
		return model;
	}

	getId(model: Model) {
		return this.#idMap.get(model);
	}

	activity(record: ActivityRecord, actor: ProfileWithHost): Activity {
		return this.#id({
			body: record.body,
			actor: this.profile(actor),
		}, record.id);
	}

	invite(record: InviteRecord) {
		return new Invite(this.#data, this.#localDomain, record);
	}
}
