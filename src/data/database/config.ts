import type Database from './database.js';

type Confs = {
	MIGRATION: number;
};

type ConfType<T extends string> = T extends keyof Confs ? Confs[T] : boolean | string | number | null;

export default class Config {
	#db: Database;

	constructor(db: Database) {
		this.#db = db;
	}

	async create() {
		await this.#db.run`CREATE TABLE IF NOT EXISTS config (
			key string UNIQUE,
			value json
		)`;
	}

	async get<K extends string>(key: K, fallback: ConfType<K>) {
		const resolved = await this.#db.get<{ key: K; value: ConfType<K> }>`SELECT * FROM config where key=${key}`;
		return resolved ? resolved.value : fallback;
	}

	async set<K extends string>(key: K, value: ConfType<K>) {
		const res = await this.#db.run`UPDATE config SET value=${value} WHERE key=${key}`;
		if (res.changes < 1) {
			await this.#db.run`INSERT INTO config (key, value) VALUES(${key}, ${value});`;
		}
	}
}
