import { PrismaClient } from '@prisma/client';
import Config from './config.js';
import Database from './database.js';
import migrate from './migrate.js';

export default async function getDatabase(file: string) {
	return new PrismaClient();
	// const db = new Database(file);
	// const config = new Config(db);
	// await config.create();

	// const v = await migrate(db, await config.get('MIGRATION', 0));
	// await config.set('MIGRATION', v);

	// return db;
}
