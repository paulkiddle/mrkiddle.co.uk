import { type Buffer } from 'node:buffer';
import { InboxInterface } from './inbox/inbox.js';

export type Domain = {
	uri: URL;
	hostname: string;
};

type Profile = {
	uri: URL;
	name: string;
	inbox: InboxInterface;
	key: {
		uri: URL;
		public: Buffer;
	};
};

export default Profile;
