import { PrismaClient } from '@prisma/client';
import { LocalActorRecord } from '../../database/migrate.js';
import type ModelFactory from '../../database/model-factory/model-factory-interface.js';
import { LocalProfile } from './user.js';

export default class Profiles {
	#db: PrismaClient;
	#mf: ModelFactory;

	constructor(db: PrismaClient, mf: ModelFactory) {
		this.#db = db;
		this.#mf = mf;
	}

	async get(username: string): Promise<LocalProfile | null> {
		const record = await this.#db.actor.findFirst({
			where: {
				name: username,
				local: true
			},
			include: {
				user: true
			}
		});
		
		return record && this.#mf.localActor(record as LocalActorRecord);
	}
}
