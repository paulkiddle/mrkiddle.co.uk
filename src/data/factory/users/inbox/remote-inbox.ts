/// <reference path="../../../../activitypub-http-signatures.d.ts"/>
import type Collection from '../../collection.js';
import { Activity } from '../../../database/model-factory/model-factory-interface.js';
import { as } from 'fediverse-terms';
import User from '../user-interface.js';

export default class RemoteInbox implements Collection<Activity> {
	#ownerUrl: URL
	#user: User

	constructor(
		user: User,
		ownerUrl: URL
	) {
		this.#user = user;
		this.#ownerUrl = ownerUrl;
	}

	async get(): Promise<Activity> {
		throw new Error('Not supported');
	}

	async getAll(): Promise<Activity[]> {
		return [await this.get()];
	}

	async add(activity: Activity) {
		const inbox = (await this.#user.get(this.#ownerUrl.href)).node(as.inbox);
		const inboxUrl = inbox?.id;
		if(!inboxUrl) {
			console.log(inbox?.toJSON());
			throw new Error('Actor has no inbox id');
		}

		if(activity.type === 'Accept') {
			const body = {
				"@context": "https://www.w3.org/ns/activitystreams",
				"type": "Accept",
				"actor": this.#user.uri.href,
				"object": activity.object.uri.href
			};

			if(await this.#user.post(inboxUrl, body)) {
				return;
			} else {
				throw new Error('Could not send signed post')
			}

		} else {
			throw new Error('Cannot post that type of activity')
		}
	}
}
