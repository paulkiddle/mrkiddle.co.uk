import type Database from '../../../database/database.js';
import { type LocalActorRecord, Id, ActivityRecord, ActorRecord } from '../../../database/database.js';
import { type Domain } from '../profile.js';
import type Profile from '../profile.js';
import type Collection from '../../collection.js';
import ModelFactory, { Activity, ActivityType } from '../../../database/model-factory/model-factory-interface.js';
import { PrismaClient } from '@prisma/client';
import { Uri } from '../../../database/model-factory/guest-model-factory.js';

type ActivityCollection = Collection<Activity>;

export type InboxWithUri = ActivityCollection & {
	uri: URL;
}

export type InboxInterface = ActivityCollection | InboxWithUri;

export class InboxBasic implements InboxWithUri {
	uri: URL;

	constructor(
		uri: URL,
	) {
		this.uri = uri;
	}

	async get(): Promise<Activity> {
		throw new Error('Not supported');
	}

	async getAll(): Promise<Activity[]> {
		return [await this.get()];
	}

	async add() {
		throw new Error('Not supported')
	}
}

type Opts = { db: PrismaClient; mf: ModelFactory; actor: Profile, uri: Uri };

export default class Inbox implements InboxWithUri {
	uri: URL;
	#db: PrismaClient;
	#owner: LocalActorRecord;
	#mf: ModelFactory;
	#actor: Profile;
	#opts: Opts

	constructor(
		opts: Opts,
		owner: LocalActorRecord,
		uri: URL,
	) {
		this.#opts = opts;
		const { db, mf, actor } = opts;
		this.uri = uri;
		this.#db = db;
		this.#owner = owner;
		this.#mf = mf;
		this.#actor = actor;
	}

	#query<T>(q: T){
		const owner = this.#mf.actor(this.#owner);
		if(owner.uri.href !== this.#actor.uri.href) {
			throw new Error('Not supported')
		}

		return {
			where: {
				...q,
				inboxes: {
					some: {
						ownerId: this.#owner.id
					}
				}
			},
			include: {
				actor: {
					include: {
						remoteActor: true,
						user: true
					}
				}
			}
		}
	}

	async getAll(): Promise<Activity[]> {
		const activities = await this.#db.activity.findMany(this.#query({})) as (ActivityRecord & { actor: ActorRecord })[];

		return activities.map(a => this.#mf.activity(a, this.#ownerUri));
	}

	get #ownerUri(){
		return this.#opts.uri.for('user', { username: this.#owner.name }).href
	}

	async get(uri: string): Promise<Activity> {
		const activity = await this.#db.activity.findFirstOrThrow(this.#query({	uri	})) as (ActivityRecord & { actor: ActorRecord });

		return this.#mf.activity(activity, this.#ownerUri);
	}

	async add(activity: Activity) {
		if (this.#actor?.uri.href !== activity.actor.uri.href) {
			throw new Error('Cannot add actvitiy by an actor other than yourself');
		}

		if(activity.type === 'Follow' && activity.object.uri.href !== this.#ownerUri) {
			throw new Error('Follow request target does not own this inbox.');
		}

		const currentActor = this.#actor;

		const { actor } = await this.#db.remoteActor.upsert({
				include: {
					actor: true
				},
				where: {
					uri:currentActor.uri.href
				},
				create: {
					uri: currentActor.uri.href,
					host: {
						connectOrCreate: {
							create: {
								origin: currentActor.uri.origin,
								hostname: currentActor.uri.hostname
							},
							where: {
								hostname: currentActor.uri.hostname
							}
						}
					},
					actor: {
						create: {
							local: false,
							name: currentActor.name,
							publicKey: currentActor.key.public,
						}
					}
				},
				update: {}
		});

		await this.#db.inboxActivity.create({
			data: {
				owner: {
					connect: {
						id: this.#owner.user.id,
					}
				},
				activity: {
					connectOrCreate: {
						create: {
							body: activity.body ?? '',
							type: activity.type,
							uri: activity.uri.href,
							actorId: actor.id 
						},
						where: {
							uri: activity.uri.href
						}
					}
				}
			}
		})
	}
}
