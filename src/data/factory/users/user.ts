/// <reference path="../../../activitypub-http-signatures.d.ts"/>
import { Sha256Signer } from 'activitypub-http-signatures';
import { clientFor, get, mime } from '../../activitypub/factory.js';
import { LocalActorRecord, UserRecord } from '../../database/migrate.js';
import { type IncomingMessage } from 'node:http';
import User from './user-interface.js';
import ModelFactory from '../../database/model-factory/model-factory-interface.js';
import { Uri } from '../../database/model-factory/guest-model-factory.js';
import { InboxWithUri } from './inbox/inbox.js';

type Data = { mf: ModelFactory, uri: Uri };

export class LocalProfile {
    uri: URL;
	name: string;
	key: {
		uri: URL;
		public: Buffer;
	};
    #mf: ModelFactory
    #record: LocalActorRecord

    constructor({ mf, uri } : Data, record: LocalActorRecord){
        this.#record = record;
        this.#mf = mf;
        this.uri = uri.for('user', { username: record.name });
        this.name = record.name,
        this.key = {
            uri: new URL('#main-key', this.uri),
            public: record.publicKey,
        }
    }

    get inbox(): InboxWithUri{
        return this.#mf.localInbox(this.#record);
    }
}

export default class LocalUser extends LocalProfile implements User  {
    #record: UserRecord

    local = true as const;

    constructor(data: Data, record: UserRecord & { actor: LocalActorRecord }){
        super(data, record.actor);

        this.#record = record
    }

    #sign(url: string, method: 'GET'|'POST' = 'GET'){
        const headers = {};

        const signer = new Sha256Signer({
            privateKey: this.#record.privateKey.toString('utf8'),
            publicKeyId: this.key.uri.href
        });
    
        const signature = signer.sign({
            url,
            method,
            headers
        });

        return {
            ...headers,
            signature
        }
    }

    async get(url: string) {
        const headers = this.#sign(url)
    
        const p = await get(url, headers);
    
        return p;
    }

    async post(url: string, body: Record<string, unknown>) {
        const headers = this.#sign(url, 'POST');
    
        const client = clientFor(url);
    
        const r = client.request(url, {
            method: 'POST',
            headers: { ...headers, 'content-type': mime.json }
        });
        r.end(JSON.stringify(body));
        
        const p = await new Promise<IncomingMessage>((resolve, reject) => {
            r.on('error', reject);
            r.on('response', res => resolve(res))
        });
    
        if(p.statusCode! >= 200 && p.statusCode! < 300) {
            return true;
        } else {
            throw new Error(`Could not send signed post to ${url}; response code ${p.statusCode}`)
        }
    }
}
