import { Node } from "../../activitypub/node.js";
import Profile from "./profile.js";

interface User extends Profile {
	local: boolean,
	post(url: string, body: Record<string, unknown>): Promise<boolean>
	get(url: string): Promise<Node>
}

export default User;
