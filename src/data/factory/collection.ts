type Collection<T> = {
	get(id: string): Promise<T | null>;
	getAll(): Promise<T[]>
	add(item: T): unknown;
};

export default Collection;
