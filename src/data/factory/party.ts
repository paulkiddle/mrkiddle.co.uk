import fs from 'node:fs/promises';

export default class Party {
	#file: string;

	constructor(file: string) {
		this.#file = file;
	}

	async set(data: string) {
		await fs.writeFile(this.#file, data);
	}

	async get() {
		return fs.readFile(this.#file, 'utf8');
	}
}
