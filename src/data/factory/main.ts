import { PrismaClient } from '@prisma/client';
import type Database from '../database/database.js';
import GuestModelFactory, { ModelFactoryArgs } from '../database/model-factory/guest-model-factory.js';
import type ModelFactory from '../database/model-factory/model-factory-interface.js';
import Invites from './invites/invites.js';
import type Party from './party.js';
import PermissionError from './permission-error.js';
import Sessions from './sessions/sessions.js';
import Profiles from './users/profiles.js';
import { Action, Body } from '../../server-types.js';
import { expand } from '../../jsonld.js';
import { as, sec } from 'fediverse-terms';
import { JsonLdArray } from 'jsonld/jsonld-spec.js';
import { NodeObject } from 'jsonld';
import Inbox from './users/inbox/inbox.js';
import { Activity, Model } from '../database/model-factory/model-factory-interface.js';
import Profile from './users/profile.js';
import UserModelFactory from '../database/model-factory/user-model-factory.js';
import { Node } from '../activitypub/node.js';
import User from './users/user-interface.js';
import { v4 } from 'uuid';
import jwt from 'jsonwebtoken';
import { Id } from '../database/database.js';
import LocalUser from './users/user.js';
import { RemoteUser } from '../activitypub/factory.js';
const key = v4();

export default class Client {
	#db: PrismaClient;
	#mf: ModelFactory;

	constructor(db: PrismaClient, mf: ModelFactory) {
		this.#db = db;
		this.#mf = mf;
	}

	get invites() {
		return new Invites(this.#db, this.#mf);
	}

	get sessions(): Sessions {
		return new Sessions(this.#db, this.#mf);
	}

	get users() {
		return new Profiles(this.#db, this.#mf);
	}

	get party(): Party {
		throw new PermissionError('Permission denied');
	}
}

export class WebClient extends Client {
	#profile: Profile | null
	#mf: ModelFactory

	constructor(db: PrismaClient, mf: ModelFactory, actor: Profile | null = null) {
		super(db, mf);

		this.#profile = actor;
		this.#mf = mf;
	}

	async parseActivity (body: Body): Promise<Activity> {
			const user = this.#profile;

			if(!user) {
				throw new Error('Not authorised to POST an activity');
			}

			const json = await body.get();
			const [j] = await expand(JSON.parse(json));
			const activity = new Node(j);
			if(!activity.nodes(as.actor).find(a => a.id === user.uri.href)) {
				console.log(`No actor with uri ${user.uri.href} in`, json);
				throw new Error('Invalid actor id')
			}
			const type = activity.types.includes(as.Follow) ? 'Follow' : null;
			const target = activity.node(as.object);

			const activityModel = {
				uri: new URL(activity.id!),
				actor: user,
				body: json,
				type: null,
			}

			if(type === 'Follow') {
				if(!target?.id) {
					throw new Error('A follow activity must have a target');
				}

				return {
					...activityModel,
					type,
					object: {
						uri: new URL(target.id)
					},
					accept(){
						throw new Error("Forbidden");
					}
				}
			}

			return activityModel;
		}

	getToken(href: string){
		return jwt.sign({
			user: this.#profile?.uri.href,
			model: href
		}, key, { expiresIn: '30m' });
	}

	verifyToken(token: string, href: string) {
		const data = jwt.verify(token, key) as Record<'user'|'model', string>;
		return data.user === this.#profile?.uri.href && data.model === href;
	}
}

const idm = new WeakMap<Model, Id>();

export type ClientFactoryArgs = Omit<ModelFactoryArgs, 'idm'>;

export class ClientFactory {
	#db: PrismaClient
	guest: WebClient
	#mfa: ModelFactoryArgs

	constructor(mfa: ClientFactoryArgs) {
		const args = { ...mfa, idm };
		this.#mfa = args;
		this.#db = mfa.db;
		this.guest = new WebClient(this.#db, new GuestModelFactory(args));
	}

	get(actor: LocalUser | RemoteUser) {
		return new WebClient(this.#db, new UserModelFactory(this.#mfa, actor), actor);
	}
}
