import { ActivityRecord, ActorRecord } from '../../database/database.js';
import ModelFactory, { FollowActivity } from '../../database/model-factory/model-factory-interface.js';
import Profile from '../users/profile.js';
import LocalUser from '../users/user.js';
type D = {
	mf: ModelFactory
}

export default class Follow implements FollowActivity {
	body?: string
	actor: Profile
	type = 'Follow' as const
	uri: URL
	object: { uri: URL }
	#user: LocalUser | null

	constructor({ mf }: D, user: LocalUser | null, record: ActivityRecord<'Follow'> & { actor: ActorRecord }, object: string){
		this.body = record.body;
		this.actor = mf.actor(record.actor);
		this.uri = new URL(record.uri)
		this.object = {
			uri: new URL(object)
		}
		this.#user = user
	}

	async accept(){
		if(this.#user?.uri.href !== this.object.uri.href) {
			throw new Error('Cannot accept an activity you\'re not the target of')
		}

		/*
		// AcceptActivityRecord = database.localActivity.create({
			targetActivityId: this.#id,
		})
		model = this.mf.acceptActivity(acceptActivityRecord)
		*/
	
		const model = {
			uri: new URL(this.#user.uri.href + '#accept-' + encodeURIComponent(this.uri.href)),
			type: 'Accept' as const,
			actor: this.#user,
			object: this
		}

		await this.actor.inbox.add(model);

		// this.#user.inbox.drop(this);
	}
}
