import { PrismaClient } from '@prisma/client';
import { type InviteRecord } from '../../database/database.js';
import type Database from '../../database/database.js';
import type ModelFactory from '../../database/model-factory/model-factory-interface.js';

export default class Invites {
	#db: PrismaClient;
	#mf: ModelFactory;

	constructor(db: PrismaClient, mf: ModelFactory) {
		this.#db = db;
		this.#mf = mf;
	}

	async get(id: string) {
		const record = await this.#db.invite.findFirst({
			where: {
				uuid: id
			}
		});

		return record && this.#mf.invite(record);
	}
}
export { default as Invite } from './invite.js';
