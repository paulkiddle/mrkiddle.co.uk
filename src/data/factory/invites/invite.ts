import argon2 from 'argon2';
import keypair from 'keypair';
import type Database from '../../database/database.js';
import { insertSession } from '../sessions/sessions.js';
import { type Id, type InviteRecord } from '../../database/migrate.js';
import { PrismaClient } from '@prisma/client';

export default class Invite {
	#db: PrismaClient;
	#record: InviteRecord;

	constructor(db: PrismaClient, r: InviteRecord) {
		this.#record = r;
		this.#db = db;
	}

	get closed() {
		const record = this.#record;
		return record.quantity === null || record.used >= record.quantity;
	}

	async register({ name, password }: { name: string; password: string }) {
		if (!/^\w+$/i.test(name)) {
			throw new TypeError('Name can only contain characters a-z 0-9 or _');
		}

		const hash = await argon2.hash(password);
		const key = keypair();
		const created = await this.#db.user.create({
			data: {
				password: hash,
				admin: this.#record.admin,
				privateKey: Buffer.from(key.private),
				actor: {
					create: {
						name,
						publicKey: Buffer.from(key.public)
					}
				}
			}
		});
		await this.#db.invite.update({
			data: {
				used: this.#record.used + 1
			},
			where: {
				id: this.#record.id
			}
		})

		return insertSession(this.#db, { id: created.id });
	}
}
