import argon2 from 'argon2';
import { v4 } from 'uuid';
import { type ProfileRecord, type UserRecord, type SessionRecord, type ProfileWithHost } from '../../database/migrate.js';
import type ModelFactory from '../../database/model-factory/model-factory-interface.js';
import { PrismaClient } from '@prisma/client';
import User from '../users/user-interface.js';
import LocalUser from '../users/user.js';
import { Uri } from '../../database/model-factory/guest-model-factory.js';

export async function insertSession(db: PrismaClient, user: Pick<UserRecord, 'id'>) {
	const sessionId = v4();
	await db.session.create({
		data: {
			uuid: sessionId,
			userId: user.id
		}
	})
	return {
		type: 'session' as const,
		uuid: sessionId,
	};
}

export default class Sessions {
	#db: PrismaClient;
	#mf: ModelFactory;

	constructor(db: PrismaClient, mf: ModelFactory) {
		this.#db = db;
		this.#mf = mf;
	}

	async get(uuid: string): Promise<{ user: LocalUser } | null> {
		const session = await this.#db.session.findFirst({
			where: {
				uuid
			},
			include: {
				user: {
					include: {
						actor: true
					}
				}
			}
		});

		if(session) {
			const user = session.user;
	
			return {
				user: this.#mf.user(user)
			}
		}
	
		return null;
	}

	async create({ name, password }: { name: string; password: string }) {
		const record = await this.#db.user.findFirst({
			where: {
				actor: {
					name
				}
			}
		});

		if (!(record && await argon2.verify(record.password, password))) {
			throw new Error('Authentication failed');
		}

		return insertSession(this.#db, record);
	}
}
