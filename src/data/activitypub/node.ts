import { NodeObject, ValueObject } from 'jsonld';

const isNode = (o: unknown): o is NodeObject => (!!o && (typeof o === 'object') && ('@id' in o));
const isValue = (v: unknown): v is ValueObject => (v != null && (typeof v === 'object') && ('@value' in v));

export class Node {
	#data: NodeObject

	constructor(data: NodeObject) {
		this.#data = data;
	}

	toJSON(){
		return this.#data;
	}

	get id(){
		const id = this.#data['@id'];
		return Array.isArray(id) ? id[0] : id;
	}

	get types(){
		const types = this.#data['@type'];
		return Array.isArray(types) ? types : [types];
	}

	*#values(key: string) {
		const k = this.#data[key];
		const v = Array.isArray(k) ? k : [k];
		for (const val of v) {
			if(isValue(val)) {
				yield val['@value']
			}
		}
	}

	value(key: string) {
		for(const v of this.#values(key)) {
			return v;
		}
	}

	values(key: string) {
		return Array.from(this.#values(key));
	}

	*#nodes(key: string) {
		const k = this.#data[key];
		const v = Array.isArray(k) ? k : [k];
		for(const n of v) {
			if(isNode(n)) {
				yield new Node(n);
			}
		}
	}

	nodes(key: string) {
		return Array.from(this.#nodes(key));
	}

	node(key: string) {
		for(const node of this.#nodes(key)) {
			return node;
		}
	}
}
