/// <reference path="../../activitypub-http-signatures.d.ts"/>
import { type IncomingMessage } from 'node:http';
import https from 'node:https';
import http from 'node:http';
import { type Signature } from 'activitypub-http-signatures';
import { as, sec } from 'fediverse-terms';
import { Node } from './node.js';
import { expand } from '../../jsonld.js';
import type Profile from '../factory/users/profile.js';
import type Inbox from '../factory/users/inbox/inbox.js';
import User from '../factory/users/user-interface.js';

export const mime: Record<string, string> = {
	json: 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
	html: 'text/html',
	htm: 'text/html',
};

export const clientFor = (url: string) => url.startsWith('http:') ? http : https;

export async function get(url: string, headers: Record<string, string> = {}) {
	const p = await new Promise<IncomingMessage>((resolve, reject) => {
		const client = clientFor(url);
		client.get(url, {
			headers: {
				...headers,
				accept: 'application/ld+json, application/json',
			},
		}, res => {
			resolve(res);
		}).on('error', reject);
	});


	if (p.statusCode! >= 200 && p.statusCode! < 300) {
		let json = '';
		for await (const chunk of p) {
			json += chunk;
		}
		
		const [body] = await expand(JSON.parse(json));
		return new Node(body);
	} else {
		throw new Error(`Get ${url} failed with ${p.statusCode}`)
	}
}

export type RemoteUser = User & { local: false };

export async function getActor(sig: Signature): Promise<RemoteUser | null> {
	const actor = await get(sig.keyId.split('#')[0]);

	const id = actor.id;

	const pubKey = actor.nodes(sec.publicKey).find(n => n.id === sig.keyId)?.value(sec.publicKeyPem)?.toString();
	if (id && pubKey && sig.verify(pubKey)) {
		const username = actor.value(as.preferredUsername) ?? id;
		const uri = new URL(id);
		return {
			local: false,
			uri,
			name: username.toString(),
			get inbox(): Inbox {
				throw new Error('Not implemented for remote actors');
			},
			key: {
				uri: new URL(sig.keyId),
				public: Buffer.from(pubKey),
			},
			post() { throw new Error('Not impemented') },
			get() { throw new Error('Not impemented') }
		};
	} else {
		// Const s = {"https://www.w3.org/ns/activitystreams#endpoints":[{"https://www.w3.org/ns/activitystreams#sharedInbox":[{"@value":"https://tech.lgbt/inbox"}]}],"@id":"https://tech.lgbt/actor","http://www.w3.org/ns/ldp#inbox":[{"@value":"https://tech.lgbt/actor/inbox"}],"https://www.w3.org/ns/activitystreams#manuallyApprovesFollowers":[{"@value":true}],"https://www.w3.org/ns/activitystreams#outbox":[{"@value":"https://tech.lgbt/actor/outbox"}],"https://www.w3.org/ns/activitystreams#preferredUsername":[{"@value":"tech.lgbt"}],"https://w3id.org/security#publicKey":[{"@id":"https://tech.lgbt/actor#main-key","https://w3id.org/security#owner":[{"@value":"https://tech.lgbt/actor"}],"https://w3id.org/security#publicKeyPem":[{"@value":"-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2FNpxAdQscgUZnAvN4ks\n2mPHhpJbVZUweOxud/2xVjhVCsCOky60pGoklcPuG9gC4gQOmc3t4hArZJAm8GqJ\nzr9UsqXXWgoLBetVyS1Mkmcxs5DTZ8jdR8Nq8Zx+CzvTQ7Bc1iuAMM2UNbbN8gb4\nbl/+VStB0jBf3dOFbeEgeWTr/5IGX/ie/ZW7kkOzYDSYVv4FZd00f9P3YNNdhazE\n4QGycFjPBFzchmRWPSIvmerETkU3c/u0gNwemGSkbwq+c9mE1u5iYWvjbEEdxCqK\nlaVjpYZBDntaWVz4TB1FmM9gI/pilUc3hO3iMZbhDSk6y4JyIkAUdG5wSpDXemLo\n+QIDAQAB\n-----END PUBLIC KEY-----\n"}]}],"@type":["https://www.w3.org/ns/activitystreams#Application"],"https://www.w3.org/ns/activitystreams#url":[{"@value":"https://tech.lgbt/about/more?instance_actor=true"}]}
		console.log('Sig: invalid body\n' + JSON.stringify(actor, null, 2));
		//console.log(actor, publicKey);
	}
	
	return null;
}
