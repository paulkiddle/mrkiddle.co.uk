import type Sessions from '../data/factory/sessions/sessions.js';
import html from '../types/html.js';
import FormController from './form-controller.js';
import form from './form.view.js';

const authView = {
	get: () => html`Login: ${form([
		html`Name <input name=name />`,
		html`Password <input name=password type=password>`,
	],
	html`<button>Log in</button>`)}`,
	parse: (u: URLSearchParams) => async (m: Sessions) => (m.create({
		name: u.get('name') ?? '',
		password: u.get('password') ?? '',
	})),
};

export default class AuthController extends FormController<Sessions> {
	constructor(model: Sessions) {
		super(model, authView);
	}
}
