import { type Action, type Controller, type Session } from '../server-types.js';
import html from '../types/html.js';
import form from './form.view.js';

type Invite = {
	closed: boolean;
	register(args: { name: string; password: string }): Promise<Session>;
};

export default class InviteController implements Controller {
	#invite: Invite;

	constructor(invite: Invite) {
		this.#invite = invite;
	}

	async run(action: Action) {
		if (action.method === 'POST') {
			const body = await action.body.getParams();
			const params = {
				name: body.get('name') ?? '',
				password: body.get('password') ?? '',
			};
			return this.#invite.register(params);
		}

		return {
			content: this.#invite.closed ? html`Closed` : form([
				html`Name <input name=name />`,
				html`Password <input name=password type=password>`,
			],
			html`<button>Signup</button>`),
		};
	}
}
