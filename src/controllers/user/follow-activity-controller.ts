import { FollowActivity } from '../../data/database/model-factory/model-factory-interface.js';
import { type Action, type Controller } from '../../server-types.js';
import html from '../../types/html.js';
import form from '../form.view.js';
import { WebClient } from '../../data/factory/main.js';


export default class FollowActivityController implements Controller {
	#model: FollowActivity;
	#user: WebClient

	constructor(user: WebClient, activity: FollowActivity) {
		this.#model = activity;
		this.#user = user;
	}
 
	async run(action: Action) {
		if (action.method === 'POST') {
			const body = await action.body.getParams();
			const token = body.get('csrf');

			if(token && this.#user.verifyToken(token, this.#model.uri.href)) {
				await this.#model.accept();
				return null
			} else {
				return this.#getView('Request forgery check failed - please submit the form again')
			}
		}

		return this.#getView()
	}

	#getView(error?: string) {
		return {
			content: html`
			${error ?? ''}
			${form([
				html`Name <input name=csrf value="${this.#user.getToken(this.#model.uri.href)}" />`,
			],
			html`<button>Accept</button>`)}`,
		};
	}
}
