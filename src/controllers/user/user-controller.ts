import { as, sec, toot } from 'fediverse-terms';
import { type Action, type Controller } from '../../server-types.js';
import html from '../../types/html.js';
import baseTemplate from '../base.view.js';
import jsonld from '../../jsonld.js';
import { LocalProfile } from '../../data/factory/users/user.js';

type K = keyof View;

const types = new Map<string, K>();

function type(mediaType: string) {
	return function (_: unknown, ctx: { name: K }) {
		types.set(mediaType, ctx.name);
	};
}

function getViewType(action: Action) {
	const keys = Array.from(types.keys());
	const type = action.accepts.mediaType(keys);
	return type && types.get(type);
}

class View {
	#profile: LocalProfile;

	constructor(p: LocalProfile) {
		this.#profile = p;
	}

	@type('text/html')
	html() {
		return {
			content: baseTemplate({ title: this.#profile.name }, html`<h1>${this.#profile.name}</h1>`),
		};
	}

	@type('application/json')
	@type('application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
	@type('application/activity+json')
	async json() {
		return {
			content: await jsonld({
				[as.id]: this.#profile.uri.href,
				[as.type]: as.Person,
				[as.preferredUsername]: this.#profile.name,
				[as.inbox]: this.#profile.inbox.uri.href,
				[as.name]: this.#profile.name,
				[as.url]: this.#profile.uri.href,
				[toot.manuallyApprovesFollowers]: true,
				[toot.discoverable]: true,
				[sec.privateKey]: {
					[as.id]: this.#profile.key.uri.href,
					[sec.owner]: this.#profile.uri.href,
					[sec.publicKeyPem]: this.#profile.key.public.toString(),
				},
			}),
		};
	}
}

export default class ProfileController implements Controller {
	#view: View;

	constructor(p: LocalProfile) {
		this.#view = new View(p);
	}

	async run(action: Action) {
		const m = getViewType(action) || 'html';
		return this.#view[m]();
	}
}
