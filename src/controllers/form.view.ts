import html, { type Html } from '../types/html.js';

export default function form(fields: Html[], buttons: Html) {
	return html`<form method='POST'>
        ${fields.map(f => html`<label>${f}</label><br>`)}
        ${buttons}
    </form>
`;
}
