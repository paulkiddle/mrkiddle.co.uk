import { type Action, type Body, type Controller } from '../server-types.js';
import type Collection from '../data/factory/collection.js';
import { Html } from '../types/html.js';

type View<Model> = (m: Model[]) => Html | Promise<Html>;;

export default class CollectionController<Model> implements Controller {
	#collection: Collection<Model>;
	#bodyParser: (b: Body) => Model | Promise<Model>;
	#view: View<Model>

	constructor(col: Collection<Model>, parse: (b: Body) => Model | Promise<Model>, view: View<Model>) {
		this.#collection = col;
		this.#bodyParser = parse;
		this.#view = view;
	}

	async run(action: Action) {
		console.log(action);
		if (action.method === 'POST') {
			await this.#collection.add(await this.#bodyParser(action.body));
		} else {
			return {
				content: await this.#view(await this.#collection.getAll())
			}
		}

		return null;
	}
}
