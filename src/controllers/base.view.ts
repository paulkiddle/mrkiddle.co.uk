import html, { type Html } from '../types/html.js';

export default function baseTemplate({ title }: { title: string }, children: Html | string) {
	return html`
		<!doctype html>
		<html>
			<head>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1" />
			<title>${title}</title>
			<style>
				html {
					height: 100%;
				}
				body{
					margin: 0;
					padding: 50px;
					gap: 50px;
					min-height:100%;
					display: flex;
					flex-direction: column;
				}
			</style>
			</head>
			<body>${children}
	`;
}
