import { type Action, type Controller, type Res, Session } from '../server-types.js';
import html, { type Html } from '../types/html.js';

type View<Model> = {
	get: (m: Model) => Html | Promise<Html>;
	parse: (u: URLSearchParams) => (m: Model) => Res | Promise<Res>;
};

export default class FormController<Model> implements Controller {
	#model: Model;
	#view: View<Model>;

	constructor(model: Model, view: View<Model>) {
		this.#model = model;
		this.#view = view;
	}

	async run(action: Action) {
		if (action.method === 'POST') {
			const body = await action.body.getParams();
			const rpcCall = this.#view.parse(body);
			return rpcCall(this.#model);
		}

		return {
			content: await this.#view.get(this.#model),
		};
	}
}
