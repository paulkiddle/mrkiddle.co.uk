import { basename } from 'node:path';
import fs from 'node:fs/promises';
import mimeTypes from 'mime-types';

export function assetsCfg(assets: string[], getAssetPathFromId: (s: string) => string) {
	const assetIdToSourceMap = Object.fromEntries(assets.map(pathname => [getAssetId(pathname), pathname]));

	function getAssetId(assetSource: string) {
		return basename(assetSource);
	}

	function getAssetPath(assetSource: string) {
		const assetId = getAssetId(assetSource);
		return getAssetPathFromId(assetId);
	}

	function getAssetSourceFromId(assetId: string) {
		return assetIdToSourceMap[assetId] || null;
	}

	return {
		assetIdToSourceMap,
		getAssetPath,
		getAssetSourceFromId,
	};
}

export class AssetController {
	#file;

	constructor(file: string) {
		this.#file = file;
	}

	async run() {
		return {
			content: {
				type: mimeTypes.lookup(this.#file) || 'application/octet-stream',
				value: await fs.readFile(this.#file),
			},
		};
	}
}
