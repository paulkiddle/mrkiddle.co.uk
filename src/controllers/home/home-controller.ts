import { type Action, type Res, type Controller } from '../../server-types.js';
import html from '../../types/html.js';

export const assets = [
	new URL('lemons.jpg', import.meta.url).pathname,
	new URL('index.css', import.meta.url).pathname,
	new URL('vrap.png', import.meta.url).pathname,
	new URL('favicon.ico', import.meta.url).pathname,
];

export default class HomeController implements Controller {
	#getAsset;

	constructor(getAsset: (s: string) => string) {
		this.#getAsset = getAsset;
	}

	run(action: Action): Res | Promise<Res> {
		return this.#html();
	}

	#html() {
		return {
			content: html`
			<!DOCTYPE html>
			<html>
			  <head>
				<title>Paul Kiddle - Develoraptor</title>
				<link rel="stylesheet" href="${this.#getAsset('index.css')}">
				<link rel="icon" href="${this.#getAsset('favicon.ico')}" />
			  </head>
			  <body>
				<header>
				  <div class="logo">
					<img src="${this.#getAsset('vrap.png')}">
					<hgroup>
					  <p>Paul Kiddle</p>
					  <h1>Develoraptor</h1>
					</hgroup>
				  </div>
				</header>
			
				<section>
					<h2>Hello</h2>
					<p>
					  My name is Paul, I'm a computer person currently living in Yorkshire.
					  I'm a proponent of small tech and decentralisation.
					  By day I work for the British Film Institute.
					</p>
				</section>
			
				<section>
				  <h2>Social & Links</h2>
				  <ul>
					<li>Email: <a href="mailto:hello@mrkiddle.co.uk">hello@mrkiddle.co.uk</a></li>
					<li>Mastodon: <a rel="me" href="https://tech.lgbt/@birdsofpraise">@birdsofpraise@tech.lgbt</a></li>
					<!--li><a href="//music.mrkiddle.co.uk">Songs I make</a></li>
					<li><a href="//blog.mrkiddle.co.uk">My occasional blog</a></li>
					<li><a href="http://www.mrkiddle.co.uk/recipes/">Recipes I like</a></li-->
					<li><a href="https://gitlab.com/paulkiddle/">GitLab</a> | <a href="https://github.com/paulkiddle/">GitHub</a></li>
				  </ul>
				</section>
			</script>
			
			  </body>
			 </html>`,
		};
	}
}
