import type Party from '../../data/factory/party.js';
import FormController from '../form-controller.js';
import view from './edit-event.js';

export default class PartyController extends FormController<Party> {
	constructor(model: Party) {
		super(model, view);
	}
}
