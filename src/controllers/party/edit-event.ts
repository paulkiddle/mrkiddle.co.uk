import type Party from '../../data/factory/party.js';
import baseTemplate from '../base.view.js';
import html from '../../types/html.js';

const eventTemplate = { async get(party: Party) {
	return baseTemplate({
		title: 'Edit Event' }, html`
		<style>
			form{
				display: flex;
				flex-direction: column;
				flex-grow: 1;
			}
			textarea {
				flex-grow: 1;
				tab-size: 2;
			}
			button {
				flex-basis: 50px;
				align-self: center;
				padding-inline: 50px;
			}
		</style>
		<form method=post>
		<textarea name="data">${await party.get()}</textarea>
		<button>Save</button>
	`);
},

parse: (formData: URLSearchParams) => async (party: Party) => {
	const data = formData.get('data');
	if (!data) {
		throw new TypeError('No data sent');
	}

	await party.set(data);
	return { content: html`Saved` };
} };

export default eventTemplate;
