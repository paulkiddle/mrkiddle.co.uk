
import { assetIdToSourceMap, getAssetPath } from './router.js';

type AssetObject = {
	name: string;
	// Content: Buffer
};

// NB: It so happens that these are currently the same, but they may change.
const assetObjectHashToSourceMap = assetIdToSourceMap;

function getAssetSourceFromObject(asset: AssetObject) {
	return assetObjectHashToSourceMap[asset.name];
}

export function getAssetPathFromObject(asset: AssetObject) {
	return getAssetPath(getAssetSourceFromObject(asset)).slice(1);
}

