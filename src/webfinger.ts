/// <reference path="webfinger.d.ts"/>

import { ActivitypubWebfinger as wf } from 'webfinger-handler';
import type Profiles from './data/factory/users/profiles.js';

export default class Webfinger {
	#host: string;
	#profiles: Profiles;

	constructor(host: string, profiles: Profiles) {
		this.#host = host;
		this.#profiles = profiles;
	}

	async #findUser(resource: { user: string; host: string }) {
		if (resource.host !== this.#host) {
			return null;
		}

		const user = await this.#profiles.get(resource.user);

		if (!user) {
			return null;
		}

		return user.uri.toString();
	}

	get handle() {
		const handle = wf(this.#findUser.bind(this));
		// Object.defineProperty(this, handle.name, {
		//     value: handle,
		//     enumerable: true
		// });
		return handle;
	}
}
