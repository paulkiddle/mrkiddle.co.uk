/// <reference path="activitypub-http-signatures.d.ts"/>
import { type IncomingMessage, type ServerResponse } from 'node:http';
import https from 'node:https';
import originalUrl from 'original-url';
import cookie from 'cookie';
import Negotiator from 'negotiator';
import sigParser from 'activitypub-http-signatures';
import { as, sec } from 'fediverse-terms';
import { type JsonLdArray } from 'jsonld/jsonld-spec.js';
import { type ValueObject } from 'jsonld';
import contentType from 'content-type';
import { expand } from './jsonld.js';
import { type Req, type Res, type Auth } from './server-types.js';

export const mime: Record<string, string> = {
	json: 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
	html: 'text/html',
	htm: 'text/html',
};

async function getAuth(req: IncomingMessage): Promise<Auth> {
	if (req.headers.cookie) {
		const uuid = cookie.parse(req.headers.cookie || '').session || null;
		if (uuid) {
			return {
				type: 'session',
				uuid,
			};
		}
	}

	if (req.headers.signature) {
		const sig = sigParser.parse(req);
		return {
			type: 'signature',
			signature: sig,
		};
	}

	return null;
}

function normalizeUrl(req: IncomingMessage) {
	const fullUrl = new URL(originalUrl(req).full!);
	fullUrl.pathname = fullUrl.pathname.replaceAll(/\/\/+/g, '/');
	const m = /(?<![./])\.([a-z\d]+)$/.exec(fullUrl.pathname);
	if (m) {
		const extName = m[1];
		const mimeType = mime[extName] ?? false;
		const ext = m[0];
		fullUrl.pathname = fullUrl.pathname.slice(0, Math.max(0, m.index));

		return {
			url: fullUrl,
			ext,
			accept: mimeType,
		};
	}

	return {
		url: fullUrl,
		ext: '',
		accept: false as const,
	};
}

class BodyParser {
	#req: AsyncIterable<Buffer | string>;

	constructor(req: AsyncIterable<Buffer | string>) {
		this.#req = req;
	}

	async #getText() {
		let body = '';
		for await (const chunk of this.#req) {
			body += chunk;
		}

		return body;
	}

	get #text() {
		return this.#getText();
	}

	async urlencoded() {
		return new URLSearchParams(await this.#text);
	}
}

export async function getRequest(req: IncomingMessage): Promise<Req> {
	const { url, accept, ext } = normalizeUrl(req);
	const headers = {
		...req.headers,
	};

	if (accept) {
		headers.accept = headers.accept ? accept + ', ' + headers.accept : accept;
	}

	const accepts = new Negotiator({ headers });

	return {
		auth: await getAuth(req),
		url,
		ext,
		action: req.method === 'POST' ? {
			method: 'POST',
			accepts,
			body: {
				...contentType.parse(req),
				async get() {
					let body = '';
					for await (const chunk of req) {
						body += chunk;
					}

					return body;
				},
				async getParams() {
					return new URLSearchParams(await this.get());
				},
			},
		} : {
			method: 'GET',
			accepts,
		},
	};
}

export function writeResponse(res: ServerResponse, response: Res) {
	res.setHeader('vary', 'accept');

	if (!response) {
		res.end();
		return;
	}

	if (response.type === 'error') {
		res.statusCode = 404;
	}

	if (response.type === 'session') {
		res.setHeader('Set-Cookie', cookie.serialize('session', response.uuid, { maxAge: 100_000_000, path: '/' }));
		res.setHeader('Location', '/');
		res.statusCode = 302;
	}

	if ('content' in response) {
		res.setHeader('content-type', response.content.type);
		res.end(response.content.value);
	} else {
		res.end();
	}
}
