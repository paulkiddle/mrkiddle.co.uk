import { type Buffer } from 'node:buffer';
import type Negotiator from 'negotiator';
import { type ParsedMediaType } from 'content-type';
import { type Signature } from 'activitypub-http-signatures';

type BaseAction<T extends string> = {
	accepts: Negotiator;
	method: T;
};

type GetAction = BaseAction<'GET'>;

export type Body = ParsedMediaType & {
	getParams(): Promise<URLSearchParams>;
	get(): Promise<string>;
};

type PostAction = {
	body: Body;
} & BaseAction<'POST'>;

export type Action = GetAction | PostAction;

export type Auth = {
	type: 'session';
	uuid: string;
} | {
	type: 'signature';
	signature: Signature;
} | null;

export type Req = {
	auth: Auth;
	url: URL;
	ext: string;
	action: Action;
};
export type Session = {
	type: 'session';
	uuid: string;
};
export type Res = null | {
	type?: 'error';
	content: {
		type: string;
		value: string | Buffer;
	};
} | Session;

export type Controller = {
	run(action: Action): Res | Promise<Res>;
};
export type Router = {
	route(url: URL, ext: string): Promise<Controller>;
};
