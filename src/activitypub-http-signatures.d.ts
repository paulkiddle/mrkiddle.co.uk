declare module 'activitypub-http-signatures' {
	import { type IncomingHttpHeaders } from 'node:http';

	export type SignerArgs = { publicKeyId: string, privateKey: string, headerNames?: string[] };

	export class Sha256Signer {
		constructor(a: SignerArgs)
		sign(a: { url: string, method: string, headers: Record<string, string> }): string
	}

	export type Signature = {
		keyId: string;
		verify(pubKey: string): boolean;
	};

	const Parser: {
		parse(req: { url?: string; method?: string; headers: IncomingHttpHeaders }): Signature;
	};

	export default Parser;
}
