import { as, sec, toot } from 'fediverse-terms';
import Jsonld, { type ContextDefinition, type JsonLdDocument } from 'jsonld';
import fs from 'fs';
import { JsonLdArray } from 'jsonld/jsonld-spec.js';

const c = {
	as: 'https://www.w3.org/ns/activitystreams',
	sec: 'https://w3id.org/security/v1',
};

const ctx = JSON.parse(fs.readFileSync(new URL('./ctx.json', import.meta.url), 'utf-8')) as Record<string, JsonLdArray>;

// const dl = (Jsonld as any).documentLoaders.node();
// function loadOriginal(url: string) {
// 	const d = await dl(url);

// 	ctx[d.documentUrl] = d.document;

// 	fs.writeFileSync(new URL('./ctx.json', import.meta.url), JSON.stringify(ctx));
// 	return d;
// }

async function documentLoader(url: string) {
	const document = (url in ctx) ? ctx[url] : {};

	return {
		documentUrl: url,
		document,
	};
}

async function compact(document: Jsonld.JsonLdDocument, context: unknown) {
	return JSON.stringify(await Jsonld.compact(document, context as ContextDefinition, {
		documentLoader,
	}));
}

export async function expand(document: unknown) {
	return Jsonld.expand(document as JsonLdDocument, { documentLoader });
}

export default async function jsonld(document: JsonLdDocument, context: string[] = Object.values(c)) {
	return {
		type: 'application/ld+json; profile="https://www.w3.org/ns/activitystreams',
		value: await compact(document, context),
	};
}
