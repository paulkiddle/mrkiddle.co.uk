declare module 'webfinger-handler' {
	import { type IncomingMessage, type ServerResponse } from 'node:http';

	export type Resource = {
		user: string;
		host: string;
	};
    type P<T> = T | Promise<T>;
    export type ResourceFetcher = (r: Resource) => P<string | null>;
    export type Handler = (req: IncomingMessage, res: ServerResponse) => Promise<boolean>;
    export function ActivitypubWebfinger(f: ResourceFetcher): Handler;
}
