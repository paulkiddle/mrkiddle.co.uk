import { match, compile } from 'path-to-regexp';

type Returns<T> = T extends (...args: any) => infer R ? R : never;

type RouterRecord<Router> = Record<keyof Router, Router[keyof Router]>;

export default function router<Router extends RouterRecord<Router>, Keys extends keyof Router = keyof Router>() {
	type FnKey<K extends Keys> = Router[K] extends (...a: any) => unknown ? K : never;

	const routes = new Map<FnKey<Keys>, string[]>();

	function route(path: string) {
		return function <K extends Keys>(fn: unknown, ctx: { name: FnKey<K>; kind: 'method'; private: false }) {
			const paths = routes.get(ctx.name);
			if (paths) {
				paths.push(path);
			} else {
				routes.set(ctx.name, [path]);
			}
		};
	}

	route.route = route;

	route.compile = function<K extends Keys>(name: FnKey<K>, ...data: Parameters<Router[K]>) {
		const [tpl] = routes.get(name)!;
		const compiled = compile(tpl, { encode: encodeURIComponent });
		const d = (0 in data ? data[0] as { [s: string]: unknown } : undefined);
		const path = compiled(d);
		return path;
	};

	route.matchAll = function * (pathname: string, ext?: string): Generator<((router: Router) => Returns<Router[Keys]>)> {
		for (const [name, path] of routes) {
			const matcher = match(path, { decode: decodeURIComponent });
			const matched = matcher(pathname) || matcher(pathname + ext);
			if (matched) {
				yield (router: Router) => router[name](matched.params);
			}
		}
	};

	route.match = function (pathname: string) {
		for (const matched of route.matchAll(pathname)) {
			return matched;
		}

		return null;
	};

	return route;
}
