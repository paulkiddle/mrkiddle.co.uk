type Source = Html | URL | string;

export type HtmlVariable = Source | Source[];

// Based on https://www.rssboard.org/rss-profile
export class Html extends String {
	type = 'text/html' as const;

	get value() {
		return this.toString();
	}

	static encode(item: HtmlVariable): string {
		if (Array.isArray(item)) {
			return item.map(Html.encode).join('');
		}

		if (item instanceof Html) {
			return item.value;
		}

		if (item instanceof URL) {
			return item.href;
		}

		return Html.cdata(item);
	}

	static cdata(source: string) {
		return source.replaceAll(/[&<>'"]/g, c => `&#x${c.charCodeAt(0).toString(16).toUpperCase()};`);
	}

	static html([lit0, ...lits]: TemplateStringsArray, ...parameters: Array<Source | Source[]>) {
		return new Html(lits.reduce((string_, lit, ix) => string_ + Html.encode(parameters[ix]) + lit, lit0));
	}
}

export default Html.html;
