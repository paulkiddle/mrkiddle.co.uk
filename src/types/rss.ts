import { Html } from './html.js';

type Source = Rss | Html | string | URL | Date;

// Based on https://www.rssboard.org/rss-profile
export class Rss extends String {
	type = 'application/rss+xml' as const;

	get value() {
		return this.toString();
	}

	static encode(item: Source | Source[]): string {
		if (Array.isArray(item)) {
			return item.map(Rss.encode).join('');
		}

		if (item instanceof Rss) {
			return item.value;
		}

		if (item instanceof Html) {
			return Rss.cdata(item.toString());
		}

		if (item instanceof URL) {
			return item.href;
		}

		if (item instanceof Date) {
			return item.toUTCString();
		}

		return Rss.cdata(item);
	}

	static cdata(source: string) {
		return (source.length < 20 || source.includes(']]>')) ? source.replaceAll(/[&<>'"]/g, c => `&#x${c.charCodeAt(0).toString(16).toUpperCase()};`) : `<![CDATA[${source}]]>`;
	}

	static rss([lit0, ...lits]: TemplateStringsArray, ...parameters: Array<Source | Source[]>) {
		return new Rss(lits.reduce((string_, lit, ix) => string_ + Rss.encode(parameters[ix]) + lit, lit0));
	}
}

export const { encode } = Rss;

export default Rss.rss;
