import { type Res, type Controller, type Router } from './server-types.js';
import baseTemplate from './controllers/base.view.js';
import InviteController from './controllers/invite-controller.js';
import AuthController from './controllers/auth-controller.js';
import HomeController, { assets } from './controllers/home/home-controller.js';
import PartyController from './controllers/party/party-controller.js';
import router from './router/main.js';
import { AssetController, assetsCfg } from './controllers/assets/main.js';
import ProfileController from './controllers/user/user-controller.js';
import FollowActivityController from './controllers/user/follow-activity-controller.js';
import CollectionController from './controllers/collection-controller.js';
import { Activity } from './data/database/model-factory/model-factory-interface.js';
import html from './types/html.js';
import { WebClient } from './data/factory/main.js';

export type Hosts = {
	root: string;
	blog: string;
	http?: true;
	port?: `:${number}`;
};

const $404: Res = {
	type: 'error',
	content: baseTemplate({ title: 'Page Not Found' }, 'Page Not Found'),
};

const $404Controller: Controller = {
	run() {
		return $404;
	},
};

const { route, compile, match } = router<Routes>();

export type Compile = typeof compile;

export { compile };

const {
	assetIdToSourceMap,
	getAssetPath,
	getAssetSourceFromId,
} = assetsCfg(assets, (assetId: string) => compile('assets', { assetId }));

export { assetIdToSourceMap, getAssetPath };

class Routes {
	#data: WebClient;

	constructor(data: WebClient) {
		this.#data = data;
	}

	@route('/')
	@route('/index')
	home(): Controller {
		return new HomeController(getAssetPath);
	}

	@route('/@:username')
	async user({ username }: { username: string }): Promise<null | Controller> {
		const user = await this.#data.users.get(username);
		return user && new ProfileController(user);
	}

	@route('/@:username/inbox')
	async inbox({ username }: { username: string }): Promise<null | CollectionController<Activity>> {
		const user = await this.#data.users.get(username);
		return user && new CollectionController(user.inbox, async (body): Promise<Activity> => {
			return this.#data.parseActivity(body);
		}, models => html`<h1>Inbox</h1>${models.map(
			m => html`<div><b>${m.type === 'Follow' ? html`<a href="${compile('activity', { username, url: m.uri.href })}">Follow</a>` : 'Unknown type'}</b><br>${m.body ?? ''}</div>`
		)}`);
	}

	@route('/@:username/inbox/:url')
	async activity({ username, url }: { username: string, url: string }) {
		const user = await this.#data.users.get(username);
		const activity = await user?.inbox.get(url);
		return activity && activity.type === 'Follow' ? new FollowActivityController(this.#data, activity) : null
	}

	@route('/assets/:assetId([a-z0-9]+\\.[a-z0-9]+)')
	assets({ assetId }: { assetId: string }) {
		const filepath = getAssetSourceFromId(assetId);
		return filepath ? new AssetController(filepath) : $404Controller;
	}

	@route('/invites/:uuid')
	async invite({ uuid }: { uuid: string }) {
		const invite = await this.#data.invites.get(uuid);

		if (invite) {
			return new InviteController(invite);
		}

		return $404Controller;
	}

	@route('/auth')
	async auth(): Promise<null | Controller> {
		return new AuthController(this.#data.sessions);
	}

	@route('/party')
	async party() {
		return new PartyController(this.#data.party);
	}
}


export default class MainRouter implements Router {
	#hosts: Hosts;
	#routes: Routes;

	constructor(hosts: Hosts, data: WebClient) {
		this.#hosts = hosts;
		this.#routes = new Routes(data);
	}

	async route(url: URL, ext: string): Promise<Controller> {
		const r = route.matchAll(url.pathname, ext);

		for (const getController of r) {
			const controller = await getController(this.#routes);
			if (controller) {
				return controller;
			}
		}

		return $404Controller;
	}
}
