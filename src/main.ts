import { type IncomingMessage, type ServerResponse } from 'node:http';
import { v4 } from 'uuid';
import { compile, type Hosts } from './router.js';
import { type Auth, type Req, type Res } from './server-types.js';
import MainRouter from './router.js';
import { ClientFactory, ClientFactoryArgs } from './data/factory/main.js';
import getDatabase from './data/database/create.js';
import { Uri } from './data/database/model-factory/guest-model-factory.js';
import Webfinger from './webfinger.js';
import { getRequest, writeResponse } from './io.js';
import { getActor } from './data/activitypub/factory.js';

export default class Server {
	#cf: ClientFactory;
	#hosts: Hosts;
	#webfinger: Webfinger;

	constructor(hosts: Hosts, mfa: ClientFactoryArgs) {
		this.#cf = new ClientFactory(mfa);
		this.#hosts = hosts;
		this.#webfinger = new Webfinger(hosts.root + hosts.port, this.#cf.guest.users);
	}

	static async create(party: string, data: string, hosts: Hosts) {
		const proto = hosts.http ? 'http://' : 'https://';
		const origin = proto + hosts.root + hosts.port;
		const db = await getDatabase(data);
		const c = await db.user.findFirst();
		if (!c) {
			const uuid = v4();
			await db.invite.create({
				data: {
					uuid,
					admin: true,
					quantity: 1
				}
			});
			console.log(`${origin}/invites/${uuid}`);
		}

		const mfa: ClientFactoryArgs = {
			db,
			uri: new Uri(origin, compile),
			origin
		}
		
		const server = new Server(hosts, mfa);
		return server.handle.bind(server);
	}

	async handle(req: IncomingMessage, res: ServerResponse) {
		if (await this.#webfinger.handle(req, res)) {
			return true;
		}

		const request = await getRequest(req);
		const response = await this.#route(request);
		writeResponse(res, response);
		return true;
	}

	async #getClient(auth: Auth) {
		if (auth?.type === 'session') {
			const session = await this.#cf.guest.sessions.get(auth.uuid);
			if (session) {
				return this.#cf.get(session.user);
			}
		}

		if (auth?.type === 'signature') {
			const actor = await getActor(auth.signature);
			if(actor) {
				return this.#cf.get(actor);
			}
		}

		return this.#cf.guest;
	}

	async #route(req: Req): Promise<Res> {
		const session = await this.#getClient(req.auth);
		const router = new MainRouter(this.#hosts, session);
		const controller = await router.route(req.url, req.ext);
		const response = await controller.run(req.action);

		return response;
	}
}

export { mime } from './io.js';
